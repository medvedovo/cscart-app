package cscart.store

import android.app.Application
import android.content.Context
import android.preference.PreferenceManager
import com.raizlabs.android.dbflow.config.FlowConfig
import com.raizlabs.android.dbflow.config.FlowManager
import cscart.store.data.repository.SharedDataRepository
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

class App : Application() {
    private lateinit var cicerone: Cicerone<Router>

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            // TODO
        }

        instance = this
        cicerone = Cicerone.create()

        SharedDataRepository.init(PreferenceManager.getDefaultSharedPreferences(applicationContext))
        FlowManager.init(FlowConfig.Builder(this).build())
    }

    override fun onTerminate() {
        super.onTerminate()
        FlowManager.destroy()
    }

    companion object {
        private lateinit var instance: App

        val context: Context
            get() = App.instance.applicationContext

        val navigatorHolder: NavigatorHolder
            get() = instance.cicerone.navigatorHolder

        val router: Router
            get() = instance.cicerone.router
    }
}