package cscart.store.data.db.dataclass

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.Index
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import cscart.store.data.db.AppDatabase

@Table(database = AppDatabase::class)
class Category(
        @PrimaryKey @Index var categoryId: String? = null,
        @Column var parentId: String? = null,
        @Column var idPath: String? = null,
        @Column var category: String? = null,
        @Column var position: String? = null,
        @Column var status: String? = null,
        @Column var companyId: String? = null,
        @Column var seoName: String? = null,
        @Column var seoPath: String? = null,
        @Column var level: Int? = null,
        @Column var hasChildren: String? = null
)