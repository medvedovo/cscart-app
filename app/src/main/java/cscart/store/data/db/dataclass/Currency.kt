package cscart.store.data.db.dataclass

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import cscart.store.data.db.AppDatabase

@Table(database = AppDatabase::class)
class Currency(
    @PrimaryKey var currencyId: Int = 0,
    @Column var currencyCode: String? = null,
    @Column var after: String? = null,
    @Column var symbol: String? = null,
    @Column var coefficient: Double? = null,
    @Column var isPrimary: String? = null,
    @Column var position: Int = 0,
    @Column var decimalsSeparator: String? = null,
    @Column var thousandsSeparator: String? = null,
    @Column var decimals: Int = 0,
    @Column var status: String? = null,
    @Column var description: String? = null
) {
    fun getIsPrimary() = isPrimary
    fun setIsPrimary(isPrimary: String) {
        this.isPrimary = isPrimary
    }
}