package cscart.store.data.db.dataclass

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import cscart.store.data.db.AppDatabase

@Table(database = AppDatabase::class)
class Language(
    @PrimaryKey var langId: Int = 0,
    @Column var langCode: String? = null,
    @Column var name: String? = null,
    @Column var status: String? = null,
    @Column var countryCode: String? = null,
    @Column var direction: String? = null,
    @Column var active: String? = null
)