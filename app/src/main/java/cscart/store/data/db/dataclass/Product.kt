package cscart.store.data.db.dataclass

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.Index
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import cscart.store.data.db.AppDatabase

@Table(database = AppDatabase::class)
class Product(
        @PrimaryKey var productId: String? = null,
        @Column @Index var product: String? = null,
        @Column var productCode: String? = null,
        @Column var price: Double? = null,
        @Column var basePrice: Double? = null,
        @Column var imageUrl: String? = null,
        @Column var mainCategoryName: String? = null,
        @Column var averageRating: String? = null
)