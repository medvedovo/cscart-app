package cscart.store.data.db.dataclass

import com.raizlabs.android.dbflow.annotation.Column
import com.raizlabs.android.dbflow.annotation.PrimaryKey
import com.raizlabs.android.dbflow.annotation.Table
import cscart.store.data.db.AppDatabase

@Table(database = AppDatabase::class)
class ProductSorting(
        @PrimaryKey var sortBy: String? = null,
        @Column var description: String? = null,
        @Column var defaultOrder: String? = null,
        @Column var isDesc: Boolean? = null
)