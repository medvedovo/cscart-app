package cscart.store.data.net

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiMethods {
    companion object {
        const val BASE_URL = "http://app-demo.ecom.cloud/"
    }

    /**
     * Настройки приложения: цвета и прочее.
     */
    @GET("index.php?dispatch=app_init.get_company_settings")
    fun getSettings(): Single<cscart.store.data.net.dataclass.companySettings.Response>

    /**
     * Список языков доступных для этого магазина.
     */
    @GET("index.php?dispatch=app_init.get_languages")
    fun getLanguages(): Single<Any>

    /**
     * Список валют доступных для этого магазина.
     */
    @GET("index.php?dispatch=app_init.get_currencies")
    fun getCurrencies(): Single<Any>

    /**
     * Список всех категорий.
     */
    @GET("index.php?dispatch=app_init.get_categories_list")
    fun getCategoriesList(): Single<cscart.store.data.net.dataclass.categoriesList.Response>

    /**
     * Список товаров по категории и разделённый на страницы.
     *
     * @param cid идентификатор категории.
     * @param page страница списка товаров.
     */
    @GET("index.php?dispatch=app_init.get_products")
    fun getProducts(
            @Query("cid") cid: String,
            @Query("page") page: Int,
            @Query("sort_by") sortBy: String? = null,
            @Query("sort_order") sortOrder: String? = null): Single<cscart.store.data.net.dataclass.productsList.Response>

    /**
     * Возможные сортировки для списка товаров
     */
    @GET("index.php?dispatch=app_init.get_product_sortings")
    fun getProductSortings(@Query("not_as_key") notAsKey: String = "Y"): Single<cscart.store.data.net.dataclass.productSortings.Response>
}