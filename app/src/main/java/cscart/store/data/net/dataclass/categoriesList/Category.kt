package cscart.store.data.net.dataclass.categoriesList

import com.google.gson.annotations.SerializedName

class Category {
    @SerializedName("category_id")
    var categoryId: String? = null

    @SerializedName("parent_id")
    var parentId: String? = null

    @SerializedName("id_path")
    var idPath: String? = null

    @SerializedName("category")
    var category: String? = null

    @SerializedName("position")
    var position: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("company_id")
    var companyId: String? = null

    @SerializedName("seo_name")
    var seoName: String? = null

    @SerializedName("seo_path")
    var seoPath: String? = null

    @SerializedName("level")
    var level: Int? = null

    @SerializedName("has_children")
    var hasChildren: String? = null
}
