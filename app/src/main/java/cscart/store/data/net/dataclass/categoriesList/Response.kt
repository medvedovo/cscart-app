package cscart.store.data.net.dataclass.categoriesList

import com.google.gson.annotations.SerializedName

class Response {
    @SerializedName("categories")
    var categories: List<Category>? = null
}
