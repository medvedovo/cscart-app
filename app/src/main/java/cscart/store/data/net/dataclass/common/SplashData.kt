package cscart.store.data.net.dataclass.common

import cscart.store.data.net.dataclass.companySettings.Appearance
import cscart.store.data.db.dataclass.Currency
import cscart.store.data.db.dataclass.Language

class SplashData {
    var settings: Appearance? = null
    var languages: List<Language>? = null
    var currencies: List<Currency>? = null
}