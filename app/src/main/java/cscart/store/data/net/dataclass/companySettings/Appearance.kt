package cscart.store.data.net.dataclass.companySettings

import com.google.gson.annotations.SerializedName

class Appearance {
    @SerializedName("bar_colors")
    var barColors: BarColors? = null
}
