package cscart.store.data.net.dataclass.companySettings

import com.google.gson.annotations.SerializedName

class BarColors {
    @SerializedName("navigation_bar_bg_color")
    var navigationBarBgColor: String? = null

    @SerializedName("navigation_bar_font_color")
    var navigationBarFontColor: String? = null

    @SerializedName("tab_bar_bg_color")
    var tabBarBgColor: String? = null

    @SerializedName("tab_bar_font_color")
    var tabBarFontColor: String? = null

    @SerializedName("bar_color_scheme")
    var barColorScheme: String? = null

    @SerializedName("navigation_bar_logo_url")
    var navigationBarLogoUrl: String? = null

    @SerializedName("rating_stars_color")
    var ratingStarsColor: String? = null
}
