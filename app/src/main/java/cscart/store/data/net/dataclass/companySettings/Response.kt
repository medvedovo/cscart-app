package cscart.store.data.net.dataclass.companySettings

import com.google.gson.annotations.SerializedName

class Response {
    @SerializedName("appearance")
    var appearance: Appearance? = null
}
