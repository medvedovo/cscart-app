package cscart.store.data.net.dataclass.productSortings

import com.google.gson.annotations.SerializedName

class Response {
    @SerializedName("sortings")
    var sortings: List<Sorting>? = null
}
