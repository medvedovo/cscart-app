package cscart.store.data.net.dataclass.productSortings

import com.google.gson.annotations.SerializedName

class Sorting {
    @SerializedName("description")
    var description: String? = null

    @SerializedName("default_order")
    var defaultOrder: String? = null

    @SerializedName("desc")
    var desc: Boolean? = null

    @SerializedName("sort_by")
    var sortBy: String? = null

}
