package cscart.store.data.net.dataclass.productsList

import com.google.gson.annotations.SerializedName

class Product {
    @SerializedName("product_id")
    var productId: String? = null

    @SerializedName("product")
    var product: String? = null

    @SerializedName("product_code")
    var productCode: String? = null

    @SerializedName("price")
    var price: Double? = null

    @SerializedName("base_price")
    var basePrice: Double? = null

    @SerializedName("image_url")
    var imageUrl: String? = null

    @SerializedName("main_category_name")
    var mainCategoryName: String? = null

    @SerializedName("average_rating")
    var averageRating: Double? = null
}
