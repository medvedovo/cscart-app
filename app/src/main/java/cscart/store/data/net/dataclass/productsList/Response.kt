package cscart.store.data.net.dataclass.productsList

import com.google.gson.annotations.SerializedName

class Response {
    @SerializedName("search")
    var search: Search? = null

    @SerializedName("products")
    var products: List<Product>? = null
}
