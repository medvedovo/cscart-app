package cscart.store.data.net.dataclass.productsList

import com.google.gson.annotations.SerializedName

class Search {
    @SerializedName("area")
    var area: String? = null

    @SerializedName("use_caching")
    var useCaching: Boolean? = null

    @SerializedName("extend")
    var extend: List<String>? = null

    @SerializedName("custom_extend")
    var customExtend: List<Any>? = null

    @SerializedName("pname")
    var pname: String? = null

    @SerializedName("pshort")
    var pshort: String? = null

    @SerializedName("pfull")
    var pfull: String? = null

    @SerializedName("pkeywords")
    var pkeywords: String? = null

    @SerializedName("feature")
    var feature: List<Any>? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("page")
    var page: Int? = null

    @SerializedName("action")
    var action: String? = null

    @SerializedName("filter_variants")
    var filterVariants: List<Any>? = null

    @SerializedName("features_hash")
    var featuresHash: String? = null

    @SerializedName("limit")
    var limit: Int? = null

    @SerializedName("bid")
    var bid: Int? = null

    @SerializedName("match")
    var match: String? = null

    @SerializedName("tracking")
    var tracking: List<Any>? = null

    @SerializedName("get_frontend_urls")
    var getFrontendUrls: Boolean? = null

    @SerializedName("items_per_page")
    var itemsPerPage: Int? = null

    @SerializedName("apply_disabled_filters")
    var applyDisabledFilters: String? = null

    @SerializedName("dispatch")
    var dispatch: String? = null

    @SerializedName("cid")
    var cid: String? = null

    @SerializedName("subcats")
    var subcats: String? = null

    @SerializedName("sort_by")
    var sortBy: String? = null

    @SerializedName("sort_order")
    var sortOrder: String? = null

    @SerializedName("sort_order_rev")
    var sortOrderRev: String? = null

    @SerializedName("total_items")
    var totalItems: Int? = null
}
