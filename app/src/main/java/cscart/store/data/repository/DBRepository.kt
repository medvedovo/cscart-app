package cscart.store.data.repository

import com.raizlabs.android.dbflow.kotlinextensions.*
import com.raizlabs.android.dbflow.sql.language.OrderBy
import com.raizlabs.android.dbflow.sql.queriable.AsyncQuery
import cscart.store.data.db.dataclass.*

object DBRepository {
    fun getCategories(parentId: String? = null): AsyncQuery<Category> {
        return (select from Category::class
                where (Category_Table.parentId eq parentId and Category_Table.status.eq("A"))
                orderBy OrderBy.fromProperty(Category_Table.position).ascending()).async()
    }

    fun countCategories(parentId: String? = null): Long {
        return (select from Category::class
                where (Category_Table.parentId eq parentId and Category_Table.status.eq("A"))).count
    }

    fun getCategory(categoryId: String?): Category? {
        return (select from Category::class where (Category_Table.categoryId eq categoryId)).querySingle()
    }

    fun getCurrencies(): AsyncQuery<Currency> {
        return (select from Currency::class
                where (Currency_Table.status eq "A")
                orderBy OrderBy.fromProperty(Currency_Table.position).ascending()).async()
    }

    fun getActiveCurrency(): Currency? {
        return (select from Currency::class
                where (Currency_Table.currencyId eq SharedDataRepository.getInt(SharedDataRepository.CURRENCY_ID)
                and Currency_Table.status.eq("A"))).querySingle()
    }

    fun getProductSortings(): List<ProductSorting> {
        return (select from ProductSorting::class).list
    }
}