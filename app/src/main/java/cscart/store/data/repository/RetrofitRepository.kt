package cscart.store.data.repository

import android.util.Log
import cscart.store.data.net.ApiMethods
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

object RetrofitRepository {
    private var callAdapterFactory = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

    private fun getOkHttpClient(): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
                .addNetworkInterceptor { chain ->
                    val builder = chain.request().newBuilder()
                    val request = builder
                            .header("Content-Type", "application/json")
                            .header("Accept", "application/json")

                    // TODO: Проверка на наличие токена авторизации
                    if (true) {
                        request.addHeader("Authorization", String.format(Locale.getDefault(), "Basic %s", "YWRtaW5AZXhhbXBsZS5jb206dmExdDkwMGIzUDJVQ1FXREhaazFNS0I4Mzk2M3oxNmk="))
                    }
                    Log.d("retrofit", "${chain.request().method()} ${chain.request().url()}")
                    chain.proceed(request.build())
                }

        okHttpClient.connectTimeout(120, TimeUnit.SECONDS)
        okHttpClient.readTimeout(120, TimeUnit.SECONDS)
        okHttpClient.writeTimeout(120, TimeUnit.SECONDS)

        return okHttpClient.build()
    }

    private fun getApiMethods(): ApiMethods {
        return Retrofit.Builder()
                .baseUrl(ApiMethods.BASE_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(callAdapterFactory)
                .build()
                .create(ApiMethods::class.java)
    }

    val repository: ApiMethods
        get() = getApiMethods()
}