package cscart.store.data.repository

import android.content.SharedPreferences

object SharedDataRepository {

    private lateinit var sharedPreferences: SharedPreferences

    fun init(sharedPreferences: SharedPreferences) {
        this.sharedPreferences = sharedPreferences
    }

    fun remove(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }

    fun saveString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    fun getString(key: String): String {
        return sharedPreferences.getString(key, "")
    }

    fun saveInt(key: String, value: Int) {
        sharedPreferences.edit().putInt(key, value).apply()
    }

    fun getInt(key: String): Int {
        return sharedPreferences.getInt(key, 0)
    }

    fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key: String): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    fun saveLong(key: String, value: Long) {
        sharedPreferences.edit().putLong(key, value).apply()
    }

    fun getLong(key: String): Long {
        return sharedPreferences.getLong(key, 0)
    }

    fun saveStringSet(key: String, data: Set<String>) {
        sharedPreferences.edit().putStringSet(key, data).apply()
    }

    fun getStringSet(key: String): Set<String>? {
        return sharedPreferences.getStringSet(key, null)
    }

    const val NAVIGATION_BAR_BG_COLOR = "navigation_bar_bg_color"
    const val TAB_BAR_BG_COLOR = "tab_bar_bg_color"
    const val TAB_BAR_FONT_COLOR = "tab_bar_font_color"
    const val NAVIGATION_BAR_LOGO_URL = "navigation_bar_logo_url"
    const val CURRENCY_ID = "currency_id"
}