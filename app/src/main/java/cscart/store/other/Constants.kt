package cscart.store.other

object Constants {
    const val EMPTY_PRICE = 1.0E-9
    const val ANIMATION_DURATION_SHORT = 200L
    const val ANIMATION_DURATION_MEDIUM = 300L
    const val ANIMATION_DURATION_LONG = 400L
}