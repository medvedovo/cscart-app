package cscart.store.other

object Fields {
    const val LANGUAGES = "languages"
    const val LANG_ID = "lang_id"
    const val LANG_CODE = "lang_code"
    const val NAME = "name"
    const val STATUS = "status"
    const val COUNTRY_CODE = "country_code"
    const val DIRECTION = "direction"
    const val ACTIVE = "active"
    const val CURRENCIES = "currencies"
    const val CURRENCY_ID = "currency_id"
    const val CURRENCY_CODE = "currency_code"
    const val AFTER = "after"
    const val SYMBOL = "symbol"
    const val COEFFICIENT = "coefficient"
    const val IS_PRIMARY = "is_primary"
    const val POSITION = "position"
    const val DECIMALS_SEPARATOR = "decimals_separator"
    const val THOUSANDS_SEPARATOR = "thousands_separator"
    const val DECIMALS = "decimals"
    const val DESCRIPTION = "description"
}