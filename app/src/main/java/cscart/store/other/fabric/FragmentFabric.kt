package cscart.store.other.fabric

import android.os.Bundle
import cscart.store.R
import cscart.store.presentation.account.AccountFragment
import cscart.store.presentation.cart.CartFragment
import cscart.store.presentation.catalog.CatalogFragment
import cscart.store.presentation.home.HomeFragment
import cscart.store.presentation.menu.MenuFragment
import cscart.store.presentation.product.ProductFragment
import cscart.store.presentation.products.ProductsFragment

object FragmentFabric {
    enum class Type {
        EMPTY,
        HOME,
        CATALOG,
        CART,
        ACCOUNT,
        MENU,

        PRODUCTS_LIST,
        PRODUCT_DETAILS;

        companion object {
            fun getTypeByName(screenName: String): Type =
                    values().firstOrNull { it.name == screenName }
                            ?: EMPTY

            fun getById(id: Int): Type = when (id) {
                R.id.action_home -> HOME
                R.id.action_catalog -> CATALOG
                R.id.action_cart -> CART
                R.id.action_account -> ACCOUNT
                R.id.action_menu -> MENU
                else -> EMPTY
            }
        }
    }

    fun create(type: Type, bundle: Bundle): IFragment? = when(type) {
        Type.HOME -> HomeFragment.newInstance(bundle)
        Type.CATALOG -> CatalogFragment.newInstance(bundle)
        Type.CART -> CartFragment.newInstance(bundle)
        Type.ACCOUNT -> AccountFragment.newInstance(bundle)
        Type.MENU -> MenuFragment.newInstance(bundle)

        Type.PRODUCTS_LIST -> ProductsFragment.newInstance(bundle)
        Type.PRODUCT_DETAILS -> ProductFragment.newInstance(bundle)

        else -> null
    }

    interface IFragment {
        val type: Type
        fun onBackPressed()
    }
}