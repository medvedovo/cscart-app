package cscart.store.other.transitions

import android.support.transition.ChangeBounds
import android.support.transition.ChangeImageTransform
import android.support.transition.ChangeTransform
import android.support.transition.TransitionSet
import cscart.store.other.Constants

class DetailsTransition : TransitionSet() {
    init {
        ordering = TransitionSet.ORDERING_TOGETHER
        addTransition(ChangeBounds()).addTransition(ChangeTransform()).addTransition(ChangeImageTransform())
        /*startDelay = Constants.ANIMATION_DURATION_MEDIUM
        duration = Constants.ANIMATION_DURATION_LONG * 2*/
    }
}
