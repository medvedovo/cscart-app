package cscart.store.other.utils

import android.util.Log
import java.lang.Exception

object ColorHelper {
    const val black = "#000000"
    
    fun convertTo6Digit(color: String): String {
        try {
            if (color.length < 7) {
                return color[0].toString() + color[1] + color[1] + color[2] + color[2] + color[3] + color[3]
            }
        } catch (e: Exception) {
            Log.e("COLOR_CONVERTER", e.message)
            return black
        }
        return color
    }
}