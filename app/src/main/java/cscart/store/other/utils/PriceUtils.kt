package cscart.store.other.utils

import cscart.store.data.repository.DBRepository
import java.text.NumberFormat
import java.util.*

object PriceUtils {
    fun getFormattedPrice(price: Double?): String? {
        if (price == null) {
            return null
        }

        val currency = DBRepository.getActiveCurrency()
                ?: return String.format(Locale.getDefault(), "%.2f", price)

        val currencyFormatter = NumberFormat.getCurrencyInstance()
        currencyFormatter.currency = Currency.getInstance(currency.currencyCode)

        return currencyFormatter.format(currency.coefficient!! * price)
    }
}