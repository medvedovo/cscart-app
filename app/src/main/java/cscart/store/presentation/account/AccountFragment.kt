package cscart.store.presentation.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter

import cscart.store.R
import cscart.store.other.fabric.FragmentFabric

class AccountFragment : MvpAppCompatFragment(), AccountView, FragmentFabric.IFragment {
    @InjectPresenter
    lateinit var presenter: AccountPresenter

    companion object {

        fun newInstance(bundle: Bundle): AccountFragment {
            val fragment = AccountFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override val type: FragmentFabric.Type
        get() = FragmentFabric.Type.ACCOUNT

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

}
