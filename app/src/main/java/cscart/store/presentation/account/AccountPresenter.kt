package cscart.store.presentation.account

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import cscart.store.App

@InjectViewState
class AccountPresenter : MvpPresenter<AccountView>() {
    fun onBackPressed() = App.router.exit()
}