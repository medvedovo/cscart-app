package cscart.store.presentation.cart

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import cscart.store.App

@InjectViewState
class CartPresenter : MvpPresenter<CartView>() {
    fun onBackPressed() = App.router.exit()
}