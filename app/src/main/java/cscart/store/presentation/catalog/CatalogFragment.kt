package cscart.store.presentation.catalog

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter

import cscart.store.R
import cscart.store.other.components.DividerDecoration
import cscart.store.other.fabric.FragmentFabric
import kotlinx.android.synthetic.main.fragment_catalog.*

class CatalogFragment : MvpAppCompatFragment(), CatalogView, FragmentFabric.IFragment {
    @InjectPresenter
    lateinit var presenter: CatalogPresenter

    companion object {
        const val PARENT_ID = "parent_id"

        fun newInstance(bundle: Bundle): CatalogFragment {
            val fragment = CatalogFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(arguments?.getString(PARENT_ID, null))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_catalog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categories_list.layoutManager = LinearLayoutManager(context)
        categories_list.addItemDecoration(DividerDecoration(context!!, R.drawable.divider, 50, 50))
        categories_list.adapter = presenter.adapter
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override val type: FragmentFabric.Type
        get() = FragmentFabric.Type.CATALOG

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

}
