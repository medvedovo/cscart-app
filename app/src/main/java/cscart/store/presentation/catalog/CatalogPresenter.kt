package cscart.store.presentation.catalog

import android.os.Bundle
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.raizlabs.android.dbflow.kotlinextensions.*
import cscart.store.App
import cscart.store.R
import cscart.store.data.db.dataclass.Category
import cscart.store.data.repository.DBRepository
import cscart.store.data.repository.RetrofitRepository
import cscart.store.other.eventbus.ChangeBackButtonVisibilityEvent
import cscart.store.other.eventbus.ChangeLoadingVisibilityEvent
import cscart.store.other.eventbus.ChangeNoDataVisibilityEvent
import cscart.store.other.eventbus.SetTitleEvent
import cscart.store.other.fabric.FragmentFabric
import cscart.store.presentation.products.ProductsFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import org.greenrobot.eventbus.EventBus

@InjectViewState
class CatalogPresenter : MvpPresenter<CatalogView>() {
    var parentId: String? = null

    fun onCreate(parentId: String? = null) {
        this.parentId = parentId
        if ((select from Category::class).count == 0L) {
            EventBus.getDefault().post(ChangeLoadingVisibilityEvent(true))
            RetrofitRepository.repository.getCategoriesList()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        it.categories?.forEach {
                            val category = Category(
                                    it.categoryId,
                                    it.parentId,
                                    it.idPath,
                                    it.category,
                                    it.position,
                                    it.status,
                                    it.companyId,
                                    it.seoName,
                                    it.seoPath,
                                    it.level,
                                    it.hasChildren
                            )
                            category.save()
                        }
                        EventBus.getDefault().post(ChangeLoadingVisibilityEvent(false))
                        displayData(parentId ?: "0")
                    }, {
                        Log.e("GET_CATEGORIES", it.message)
                        EventBus.getDefault().post(ChangeLoadingVisibilityEvent(false))
                        EventBus.getDefault().post(ChangeNoDataVisibilityEvent(true))
                    })
        } else {
            displayData(parentId ?: "0")
        }
    }

    fun onResume() {
        var title = App.context.getString(R.string.title_catalog)
        if (!parentId.isNullOrEmpty() && parentId != "0") {
            val category = DBRepository.getCategory(parentId)
            title = category?.category
        }
        EventBus.getDefault().post(SetTitleEvent(title))
        EventBus.getDefault().post(ChangeBackButtonVisibilityEvent(!parentId.isNullOrEmpty() && parentId != "0"))
    }

    private fun displayData(parentId: String) {
        DBRepository.getCategories(parentId) list { _, data ->
            adapter.parentId = parentId
            adapter.data = data
            adapter.notifyDataSetChanged()
        }
    }

    val adapter: CategoriesAdapter = CategoriesAdapter(object : CategoriesAdapter.IClickListener {
        override fun onItemClick(id: String?, isProducts: Boolean) {
            val bundle = Bundle()

            if (DBRepository.countCategories(id) > 0 && !isProducts) {
                bundle.putString(CatalogFragment.PARENT_ID, id)
                App.router.navigateTo(FragmentFabric.Type.CATALOG.name, bundle)
            } else {
                bundle.putString(ProductsFragment.CATEGORY_ID, id)
                App.router.navigateTo(FragmentFabric.Type.PRODUCTS_LIST.name, bundle)
            }
        }
    })

    fun onBackPressed() = App.router.exit()
}