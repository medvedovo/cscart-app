package cscart.store.presentation.catalog

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import cscart.store.App
import cscart.store.R
import cscart.store.data.db.dataclass.Category

class CategoriesAdapter(val clickListener: IClickListener) : RecyclerView.Adapter<CategoriesAdapter.CategoryHolder>() {
    var parentId: String = "0"
    var data: List<Category>? = null

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CategoryHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_category, parent, false)
        return CategoryHolder(view)
    }

    override fun getItemCount(): Int = data?.size?.plus(1) ?: 0

    override fun onBindViewHolder(holder: CategoryHolder, position: Int) {
        if (position > 0) {
            val category = data?.get(position - 1)
            holder.id = category?.categoryId
            holder.categoryName.text = category?.category
        } else {
            holder.id = parentId
            holder.categoryName.text = App.context.getString(R.string.view_all_products)
            holder.isProducts = true
        }
    }

    interface IClickListener {
        fun onItemClick(id: String?, isProducts: Boolean)
    }

    inner class CategoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var id: String? = null
        val categoryName: TextView = itemView.findViewById(R.id.category_name)
        var isProducts: Boolean = false

        init {
            itemView.setOnClickListener({
                clickListener.onItemClick(id, isProducts)
            })
        }
    }
}