package cscart.store.presentation.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter

import cscart.store.R
import cscart.store.other.fabric.FragmentFabric

class HomeFragment : MvpAppCompatFragment(), HomeView, FragmentFabric.IFragment {
    @InjectPresenter
    lateinit var presenter: HomePresenter

    companion object {

        fun newInstance(bundle: Bundle): HomeFragment {
            val fragment = HomeFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    override val type: FragmentFabric.Type
        get() = FragmentFabric.Type.HOME

    override fun onBackPressed() {
        presenter.onBackPressed()
    }

}
