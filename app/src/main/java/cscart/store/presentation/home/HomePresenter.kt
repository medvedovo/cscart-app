package cscart.store.presentation.home

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import cscart.store.App

@InjectViewState
class HomePresenter : MvpPresenter<HomeView>() {
    fun onBackPressed() = App.router.exit()
}