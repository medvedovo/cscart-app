package cscart.store.presentation.main

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import cscart.store.R
import cscart.store.data.repository.SharedDataRepository
import kotlinx.android.synthetic.main.activity_main.*
import cscart.store.other.utils.BottomNavigationViewHelper
import cscart.store.other.utils.ColorHelper

class MainActivity : MvpAppCompatActivity(), MainView {

    @InjectPresenter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.onCreate(supportFragmentManager)
        bottom_navigation.setOnNavigationItemSelectedListener(presenter.getBottomNavigationListener)
        BottomNavigationViewHelper.removeShiftMode(bottom_navigation)

        val topBarColor = ColorHelper.convertTo6Digit(SharedDataRepository.getString(SharedDataRepository.NAVIGATION_BAR_BG_COLOR))
        val bottomBarColor = ColorHelper.convertTo6Digit(SharedDataRepository.getString(SharedDataRepository.TAB_BAR_BG_COLOR))
        val bottomTextColor = ColorHelper.convertTo6Digit(SharedDataRepository.getString(SharedDataRepository.TAB_BAR_FONT_COLOR))
        if (!topBarColor.isEmpty() && !bottomBarColor.isEmpty() && !bottomTextColor.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = Color.parseColor(topBarColor)
                //window.navigationBarColor = Color.parseColor(bottomBarColor)
            }
            supportActionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor(topBarColor)))
            val state = arrayOf(intArrayOf(-android.R.attr.state_enabled),
                    intArrayOf(android.R.attr.state_enabled),
                    intArrayOf(-android.R.attr.state_checked),
                    intArrayOf(android.R.attr.state_pressed)
            )
            val fontColor = Color.parseColor(bottomTextColor)
            val color = intArrayOf(fontColor, fontColor, fontColor, fontColor)
            bottom_navigation.itemTextColor = ColorStateList(state, color)
            bottom_navigation.itemIconTintList = ColorStateList(state, color)
            bottom_navigation.setBackgroundColor(Color.parseColor(bottomBarColor))
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun showSystemMessage(message: String?) {
        Snackbar.make(content, message ?: "", Snackbar.LENGTH_LONG).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun changeNoDataLayoutVisibility(visible: Boolean) {
        if (visible) {
            supportActionBar?.hide()
            no_data_layout.visibility = View.VISIBLE
        } else {
            supportActionBar?.show()
            no_data_layout?.visibility = View.GONE
        }
    }

    override fun changeLoadingLayoutVisibility(visible: Boolean) {
        loading_layout.visibility = if (visible) View.VISIBLE else View.GONE
    }

    override fun setTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun changeBackButtonVisibility(visible: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(visible)
    }
}
