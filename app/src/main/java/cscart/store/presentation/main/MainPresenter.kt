package cscart.store.presentation.main

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.transition.Fade
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.widget.ImageView
import android.widget.TextView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import cscart.store.App
import cscart.store.R
import cscart.store.other.eventbus.ChangeBackButtonVisibilityEvent
import cscart.store.other.eventbus.ChangeLoadingVisibilityEvent
import cscart.store.other.eventbus.ChangeNoDataVisibilityEvent
import cscart.store.other.eventbus.SetTitleEvent
import cscart.store.other.fabric.FragmentFabric
import cscart.store.other.transitions.DetailsTransition
import cscart.store.presentation.product.ProductFragment
import cscart.store.presentation.products.ProductsFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportFragmentNavigator
import ru.terrakok.cicerone.commands.Command

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {
    private lateinit var navigator: Navigator
    private lateinit var fragmentManager: FragmentManager

    fun onCreate(fragmentManager: FragmentManager) {
        this.fragmentManager = fragmentManager
        this.navigator = object : SupportFragmentNavigator(fragmentManager, R.id.content) {
            override fun createFragment(screenKey: String, data: Any?): Fragment {
                return FragmentFabric.create(FragmentFabric.Type.getTypeByName(screenKey), data as? Bundle
                        ?: Bundle()) as Fragment
            }

            override fun exit() {
                viewState.finish()
            }

            override fun showSystemMessage(message: String?) {
                viewState.showSystemMessage(message)
            }

            override fun setupFragmentTransactionAnimation(command: Command?, currentFragment: Fragment?, nextFragment: Fragment?, fragmentTransaction: FragmentTransaction?) {
                if (currentFragment is ProductsFragment && nextFragment is ProductFragment) {
                    nextFragment.sharedElementEnterTransition = DetailsTransition()
                    nextFragment.sharedElementReturnTransition = DetailsTransition()
                    nextFragment.enterTransition = Fade()
                    currentFragment.exitTransition = Fade()
                    fragmentTransaction?.addSharedElement(
                            currentFragment.presenter.currentHolder?.findViewById<ImageView>(R.id.product_image), "productImage")
                    fragmentTransaction?.addSharedElement(
                            currentFragment.presenter.currentHolder?.findViewById<TextView>(R.id.product_name), "productName")
                }
                super.setupFragmentTransactionAnimation(command, currentFragment, nextFragment, fragmentTransaction)
            }
        }
        App.navigatorHolder.setNavigator(navigator)
        App.router.newRootScreen(FragmentFabric.Type.HOME.name)
    }

    fun onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this)
        }
    }

    fun onStop() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this)
        }
    }

    val getBottomNavigationListener: BottomNavigationView.OnNavigationItemSelectedListener =
            BottomNavigationView.OnNavigationItemSelectedListener {
                App.router.newRootScreen(FragmentFabric.Type.getById(it.itemId).name)
                true
            }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: ChangeNoDataVisibilityEvent) {
        viewState.changeNoDataLayoutVisibility(event.visible)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: ChangeLoadingVisibilityEvent) {
        viewState.changeLoadingLayoutVisibility(event.visible)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: SetTitleEvent) {
        viewState.setTitle(event.title)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: ChangeBackButtonVisibilityEvent) {
        viewState.changeBackButtonVisibility(event.visible)
    }
}