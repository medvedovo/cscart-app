package cscart.store.presentation.main

import com.arellomobile.mvp.MvpView

interface MainView : MvpView {
    fun finish()
    fun showSystemMessage(message: String?)
    fun changeNoDataLayoutVisibility(visible: Boolean)
    fun changeLoadingLayoutVisibility(visible: Boolean)
    fun setTitle(title: String)
    fun changeBackButtonVisibility(visible: Boolean)
}