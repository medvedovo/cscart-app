package cscart.store.presentation.menu

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import cscart.store.App

@InjectViewState
class MenuPresenter : MvpPresenter<MenuView>() {
    fun onBackPressed() = App.router.exit()
}