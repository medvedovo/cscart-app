package cscart.store.presentation.product

import android.os.Bundle
import android.support.transition.Fade
import android.view.*
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import cscart.store.R
import cscart.store.other.fabric.FragmentFabric
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import cscart.store.App
import cscart.store.other.Constants
import cscart.store.other.transitions.DetailsTransition
import kotlinx.android.synthetic.main.fragment_product.*

class ProductFragment : MvpAppCompatFragment(), ProductView, FragmentFabric.IFragment {
    @InjectPresenter
    lateinit var presenter: ProductPresenter

    companion object {
        const val PRODUCT_ID = "product_id"

        fun newInstance(bundle: Bundle): ProductFragment {
            val fragment = ProductFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(arguments?.getString(PRODUCT_ID, null))
        /*enterTransition = Fade().setDuration(Constants.ANIMATION_DURATION_SHORT)
        exitTransition = Fade().setDuration(Constants.ANIMATION_DURATION_SHORT)
        sharedElementEnterTransition = DetailsTransition()
        sharedElementReturnTransition = DetailsTransition()*/
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_product, container, false)
    }

    override fun onResume() {
        super.onResume()
        //presenter.onResume()
    }

    override fun setImage(url: String) {
        Glide.with(App.context)
                .load(url)
                .apply(RequestOptions()
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .into(product_image)
    }

    override fun setName(name: String) {
        product_name.text = name
    }

    override val type: FragmentFabric.Type
        get() = FragmentFabric.Type.PRODUCT_DETAILS

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
