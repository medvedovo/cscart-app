package cscart.store.presentation.product

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import cscart.store.App

@InjectViewState
class ProductPresenter: MvpPresenter<ProductView>() {
    fun onCreate(id: String?) {
        // TODO: Получать данные из API
        viewState.setImage("http://app-demo.ecom.cloud/images/thumbnails/420/420/detailed/0/led-tv-42TL515-01.jpg")
        viewState.setName("3D LED HD телевизор Toshiba 42TL515U  Class 1080P 42\"")
    }

    fun onBackPressed() = App.router.exit()
}