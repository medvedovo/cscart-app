package cscart.store.presentation.product

import com.arellomobile.mvp.MvpView

interface ProductView: MvpView {
    fun setImage(url: String)
    fun setName(name: String)
}