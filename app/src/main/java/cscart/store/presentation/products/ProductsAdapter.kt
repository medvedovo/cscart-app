package cscart.store.presentation.products

import android.graphics.Paint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import cscart.store.App
import cscart.store.R
import cscart.store.data.net.dataclass.productsList.Product
import cscart.store.other.Constants
import cscart.store.other.utils.PriceUtils
import java.util.*

class ProductsAdapter(val clickListener: IClickListener) : RecyclerView.Adapter<ProductsAdapter.ProductHolder>() {
    var data: MutableList<Product>? = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ProductHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_product, parent, false)
        return ProductHolder(view)
    }

    override fun getItemCount(): Int = data?.size ?: 0

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        val product = data?.get(position) ?: return
        holder.id = product.productId
        holder.productRating.rating = product.averageRating?.toFloat() ?: 0.toFloat()
        holder.productName.text = product.product
        holder.productPrice.text = PriceUtils.getFormattedPrice(product.price)
        if (product.basePrice != null && product.basePrice!! > 0 && product.basePrice != Constants.EMPTY_PRICE) {
            holder.basePrice.visibility = View.VISIBLE
            holder.basePrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            holder.basePrice.text = PriceUtils.getFormattedPrice(product.basePrice)

            if (product.price != null && product.basePrice!! > 0) {
                val discount = (1 - product.price!! / product.basePrice!!) * -100
                holder.productDiscount.visibility = View.VISIBLE
                holder.productDiscount.text = String.format(Locale.getDefault(), "%.0f%%", discount)
            }
        } else {
            holder.basePrice.visibility = View.GONE
            holder.productDiscount.visibility = View.GONE
        }

        Glide.with(App.context)
                .load(product.imageUrl)
                .apply(RequestOptions()
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
                .into(holder.productImage)
    }

    interface IClickListener {
        fun onItemClick(id: String?, view: View)
    }

    inner class ProductHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var id: String? = null
        val productImage: ImageView = itemView.findViewById(R.id.product_image)
        val productRating: RatingBar = itemView.findViewById(R.id.product_rating)
        val productName: TextView = itemView.findViewById(R.id.product_name)
        val productPrice: TextView = itemView.findViewById(R.id.product_price)
        val basePrice: TextView = itemView.findViewById(R.id.base_price)
        val productDiscount: TextView = itemView.findViewById(R.id.product_discount)

        init {
            itemView.setOnClickListener({
                clickListener.onItemClick(id, itemView)
            })
        }
    }
}