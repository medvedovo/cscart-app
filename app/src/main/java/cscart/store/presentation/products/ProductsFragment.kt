package cscart.store.presentation.products

import android.os.Bundle
import android.support.transition.Fade
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import cscart.store.R
import cscart.store.other.components.EndlessRecyclerViewScrollListener
import cscart.store.other.fabric.FragmentFabric
import kotlinx.android.synthetic.main.fragment_products.*
import android.view.animation.AnimationUtils
import cscart.store.other.Constants

class ProductsFragment : MvpAppCompatFragment(), ProductsView, FragmentFabric.IFragment {
    @InjectPresenter
    lateinit var presenter: ProductsPresenter

    companion object {
        const val CATEGORY_ID = "category_id"

        fun newInstance(bundle: Bundle): ProductsFragment {
            val fragment = ProductsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onCreate(arguments?.getString(CATEGORY_ID, null))
        setHasOptionsMenu(true)
        //exitTransition = Fade().setDuration(Constants.ANIMATION_DURATION_MEDIUM / 2L)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        products_list.layoutManager = layoutManager
        products_list.adapter = presenter.adapter
        products_list.addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (totalItemsCount < presenter.totalItems) {
                    presenter.getData()
                } else {
                    products_list.clearOnScrollListeners()
                }
            }

        })
    }

    override fun onResume() {
        super.onResume()
        //presenter.onResume()
    }

    override fun updateRecyclerView() {
        val context = products_list.context
        val controller = AnimationUtils.loadLayoutAnimation(context, R.anim.grid_layout_animation_from_bottom)

        products_list.layoutAnimation = controller
        products_list.adapter.notifyDataSetChanged()
        products_list.scheduleLayoutAnimation()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.actionbar_products_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_sorting -> {
                if (!item.subMenu.hasVisibleItems()) {
                    presenter.getSortings(item.subMenu)
                }
            }
            //R.id.action_filter -> {}
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override val type: FragmentFabric.Type
        get() = FragmentFabric.Type.PRODUCTS_LIST

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
