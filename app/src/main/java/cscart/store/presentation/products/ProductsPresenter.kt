package cscart.store.presentation.products

import android.os.Bundle
import android.support.transition.FragmentTransitionSupport
import android.util.Log
import android.view.SubMenu
import android.view.View
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.raizlabs.android.dbflow.kotlinextensions.save
import cscart.store.App
import cscart.store.R
import cscart.store.data.db.dataclass.ProductSorting
import cscart.store.data.repository.DBRepository
import cscart.store.data.repository.RetrofitRepository
import cscart.store.other.eventbus.ChangeBackButtonVisibilityEvent
import cscart.store.other.eventbus.ChangeLoadingVisibilityEvent
import cscart.store.other.eventbus.ChangeNoDataVisibilityEvent
import cscart.store.other.eventbus.SetTitleEvent
import cscart.store.other.fabric.FragmentFabric
import cscart.store.presentation.product.ProductFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import org.greenrobot.eventbus.EventBus

@InjectViewState
class ProductsPresenter : MvpPresenter<ProductsView>() {
    private var categoryId: String? = null
    private var page: Int = 1
    var totalItems: Int = 0
    private var itemPerPage: Int = 0
    private var sortBy: String? = null
    private var sortOrder: String? = null

    var currentHolder: View? = null

    private val ASC = "asc"
    private val DESC = "desc"

    fun onCreate(categoryId: String? = null) {
        this.categoryId = categoryId
        if (categoryId.isNullOrEmpty()) {
            EventBus.getDefault().post(ChangeNoDataVisibilityEvent(true))
        } else {
            val category = DBRepository.getCategory(categoryId)
            EventBus.getDefault().post(SetTitleEvent(category?.category
                    ?: App.context.getString(R.string.title_products)))
            EventBus.getDefault().post(ChangeBackButtonVisibilityEvent(true))
            getData()
        }
    }

    fun getData() {
        EventBus.getDefault().post(ChangeLoadingVisibilityEvent(true))
        RetrofitRepository.repository.getProducts(categoryId!!, page, sortBy, sortOrder)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    this.page = it.search?.page!! + 1
                    this.totalItems = it.search?.totalItems!!
                    this.itemPerPage = it.search?.itemsPerPage!!
                    it.products?.forEach {
                        adapter.data?.add(it)
                    }
                    EventBus.getDefault().post(ChangeLoadingVisibilityEvent(false))
                    viewState.updateRecyclerView()
                }, {
                    Log.e("GET_PRODUCTS", it.message)
                    EventBus.getDefault().post(ChangeNoDataVisibilityEvent(true))
                    EventBus.getDefault().post(ChangeLoadingVisibilityEvent(false))
                })
    }

    /**
     * Получает доступные сортировки и сохраняет их в БД.
     */
    fun getSortings(menu: SubMenu) {
        if (DBRepository.getProductSortings().isEmpty()) {
            RetrofitRepository.repository.getProductSortings()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        it.sortings?.forEach {
                            ProductSorting(
                                    it.sortBy,
                                    it.description,
                                    it.defaultOrder,
                                    it.desc
                            ).save()
                            fillSortingMenu(menu)
                        }
                    }, {
                        Log.e("GET_PRODUCT_SORTINGS", it.message)
                    })
        } else {
            fillSortingMenu(menu)
        }
    }

    private fun fillSortingMenu(menu: SubMenu) {
        DBRepository.getProductSortings().forEach { sorting ->
            menu.add(sorting.description)
                    .setIcon(getSortingIcon(sorting.defaultOrder))
                    .setOnMenuItemClickListener {
                        page = 1
                        if (sorting.sortBy == "null") {
                            sortBy = null
                            sortOrder = null
                        } else {
                            sortOrder = if (sortBy == sorting.sortBy) {
                                if (sortOrder == ASC) DESC else ASC
                            } else {
                                sorting.defaultOrder
                            }
                            sortBy = sorting.sortBy
                            it.setIcon(getSortingIcon(sortOrder))
                        }
                        adapter.data?.clear()
                        getData()
                        true
                    }
        }
    }

    private fun getSortingIcon(order: String?): Int = if (order == ASC) R.drawable.ic_arrow_downward_black_18dp else R.drawable.ic_arrow_upward_black_18dp

    val adapter: ProductsAdapter = ProductsAdapter(object : ProductsAdapter.IClickListener {
        override fun onItemClick(id: String?, view: View) {
            currentHolder = view
            val bundle = Bundle()
            bundle.putString(ProductFragment.PRODUCT_ID, id)
            App.router.navigateTo(FragmentFabric.Type.PRODUCT_DETAILS.name, bundle)
        }
    })

    fun onBackPressed() = App.router.exit()
}