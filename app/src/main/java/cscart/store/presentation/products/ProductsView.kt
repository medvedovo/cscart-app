package cscart.store.presentation.products

import com.arellomobile.mvp.MvpView

interface ProductsView : MvpView {
    fun updateRecyclerView()
}