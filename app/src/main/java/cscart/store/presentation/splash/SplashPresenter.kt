package cscart.store.presentation.splash

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.raizlabs.android.dbflow.kotlinextensions.save
import cscart.store.data.net.dataclass.common.SplashData
import cscart.store.data.net.dataclass.companySettings.Response
import cscart.store.data.db.dataclass.Currency
import cscart.store.data.db.dataclass.Language
import cscart.store.data.repository.RetrofitRepository
import cscart.store.data.repository.SharedDataRepository
import cscart.store.other.Fields
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Function3
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

@InjectViewState
class SplashPresenter : MvpPresenter<SplashView>() {
    fun onCreate() {
        Single.zip(RetrofitRepository.repository.getSettings(),
                RetrofitRepository.repository.getLanguages(),
                RetrofitRepository.repository.getCurrencies(),
                Function3<Response, Any, Any, SplashData> { settings, languages, currencies ->
                    val result = SplashData()

                    result.settings = settings.appearance

                    // Сериализация языков
                    val languagesList = ArrayList<Language>()
                    val gsonLanguages = (JSONObject.wrap(languages) as JSONObject).getJSONObject(Fields.LANGUAGES)
                    gsonLanguages.keys().forEach {
                        val jsonLang = (gsonLanguages[it] as JSONObject)
                        val language = Language()
                        language.langId = jsonLang.getInt(Fields.LANG_ID)
                        language.langCode = jsonLang.getString(Fields.LANG_CODE)
                        language.name = jsonLang.getString(Fields.NAME)
                        language.status = jsonLang.getString(Fields.STATUS)
                        language.countryCode = jsonLang.getString(Fields.COUNTRY_CODE)
                        language.direction = jsonLang.getString(Fields.DIRECTION)
                        language.active = jsonLang.getString(Fields.ACTIVE)
                        languagesList.add(language)
                    }
                    result.languages = languagesList

                    // Сериализация валют
                    val currenciesList = ArrayList<Currency>()
                    val gsonCurrencies = (JSONObject.wrap(currencies) as JSONObject).getJSONObject(Fields.CURRENCIES)
                    gsonCurrencies.keys().forEach {
                        val jsonCurrency = (gsonCurrencies[it] as JSONObject)
                        val currency = Currency()
                        currency.currencyId = jsonCurrency.getInt(Fields.CURRENCY_ID)
                        currency.currencyCode = jsonCurrency.getString(Fields.CURRENCY_CODE)
                        currency.after = jsonCurrency.getString(Fields.AFTER)
                        currency.symbol = jsonCurrency.getString(Fields.SYMBOL)
                        currency.coefficient = jsonCurrency.getDouble(Fields.COEFFICIENT)
                        currency.isPrimary = jsonCurrency.getString(Fields.IS_PRIMARY)
                        currency.position = jsonCurrency.getInt(Fields.POSITION)
                        currency.decimalsSeparator = jsonCurrency.getString(Fields.DECIMALS_SEPARATOR)
                        currency.thousandsSeparator = jsonCurrency.getString(Fields.THOUSANDS_SEPARATOR)
                        currency.decimals = jsonCurrency.getInt(Fields.DECIMALS)
                        currency.status = jsonCurrency.getString(Fields.STATUS)
                        currency.description = jsonCurrency.getString(Fields.DESCRIPTION)
                        currenciesList.add(currency)
                    }
                    result.currencies = currenciesList

                    result
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    SharedDataRepository.saveString(SharedDataRepository.NAVIGATION_BAR_LOGO_URL, it.settings?.barColors?.navigationBarLogoUrl ?: "")
                    SharedDataRepository.saveString(SharedDataRepository.NAVIGATION_BAR_BG_COLOR, it.settings?.barColors?.navigationBarBgColor ?: "")
                    SharedDataRepository.saveString(SharedDataRepository.TAB_BAR_BG_COLOR, it.settings?.barColors?.tabBarBgColor ?: "")
                    SharedDataRepository.saveString(SharedDataRepository.TAB_BAR_FONT_COLOR, it.settings?.barColors?.tabBarFontColor ?: "")
                    it.languages?.forEach({
                        it.save()
                    })
                    it.currencies?.forEach({
                        it.save()
                        if (it.isPrimary == "Y") {
                            SharedDataRepository.saveInt(SharedDataRepository.CURRENCY_ID, it.currencyId)
                        }
                    })
                    viewState.redirectToApp()
                }, {
                    Log.e("SPLASH", it.message)
                })
    }
}