package cscart.store.presentation.splash

import com.arellomobile.mvp.MvpView

interface SplashView : MvpView {
    fun redirectToApp()
}